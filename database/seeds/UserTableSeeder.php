<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = factory('App\Company')->create();

        $user = factory(\App\User::class,4)->create(['company_id' => $company->id]);

    }
}
