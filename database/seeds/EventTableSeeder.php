<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        $user = factory('App\User')->create();

        $event = factory(\App\Event::class,6)->create(['user_id' => $user->id]);
    }
}
