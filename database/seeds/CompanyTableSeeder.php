<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $company = factory('App\Company')->create();
        $company = factory(\App\Company::class,4)->create();
    }
}
