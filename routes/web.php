<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UserController@index');
Route::get('/addUser', 'UserController@create');
Route::post('/addUser', 'UserController@store');
Route::get('/', 'UserController@display');

Route::get('/editUser/{id}', 'UserController@edit');
Route::patch('/editUser/{id}', 'UserController@update');
Route::delete('/users/{id}', 'UserController@destroy');
Route::get('/users/{id}/userDetails', 'UserController@show');
