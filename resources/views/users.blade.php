@extends('layouts.app')

@section('content')
<div>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header text-dark">Users
                    <a href="/addUser" class="btn btn-success float-right text-light">Add New User</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                           <p>{{\Session::get('success') }}</p>
                        </div>
                    @endif
                    @if(\Session::has('danger'))
                        <div class="alert alert-danger">
                           <p>{{\Session::get('danger') }}</p>
                        </div>
                    @endif
                      <table id="example" class="table table-hover table-bordered">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">First Name </th>
                            <th scope="col">Last Name </th>
                            <th scope="col">Company Name</th>
                            <th class="align-middle" colspan="2" >Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($members as $member)
                                <tr>
                                  <td>{{$member->first_name}}</td>
                                  <td>{{$member->last_name}}</td>
                                  <td>{{$member->company->name}}</td>
                                  <td width="180px"><a href="{{action('UserController@edit', $member['id'])}}" class="btn btn-primary float-left">
                                        <i class="fa fa-edit"></i> Edit</a>
                                        <form method="POST" class="delete_form" action="{{action('UserController@destroy', $member['id'])}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" class="btn btn-danger float-left ml-2"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </td>
                                    <td width="100px">
                                        <a href="{{url('/users/'.$member->id.'/userDetails')}}" class="btn btn-info ml-2 active"><i class="fa fa-eye"></i>View</a>
                                  </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.20/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.20/datatables.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        // Setup - add a text input to each footer cell
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
    });
</script>
<script>
        $(document).ready(function(){

            $('.delete_form').on('submit', function(){
                if(confirm('Are you sure you want to delete it?'))
                {
                    return true;
                }else
                {
                    return false;
                }
            });

            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 ); // 5 secs
        });
</script>
@endsection


