@extends('layouts.app')

@section('content')
<div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-dark">
                    <a class="btn btn-primary float-left" href="/"><i class="fa fa-arrow-left "></i> Back</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                           <p>{{\Session::get('success') }}</p>
                        </div>
                    @endif
                    @if(\Session::has('danger'))
                        <div class="alert alert-danger">
                           <p>{{\Session::get('danger') }}</p>
                        </div>
                    @endif
                      <table id="example" class="table table-hover table-bordered">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">User Name </th>
                            <th scope="col">Company Name</th>
                            <th scope="col">Events</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($event as $member)
                            <tr>
                              <td>{{$member->user->first_name}}</td>
                              <td>{{$company->name}}</td>
                              <td>{{$member->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.20/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.20/datatables.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        // Setup - add a text input to each footer cell
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true
        } );
    });
</script>
@endsection


