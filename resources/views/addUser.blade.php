@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-secondary text-light">Add New User</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container">
                        @if(count($errors)> 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                               <p>{{\Session::get('success') }}</p>
                            </div>
                        @endif
                        <form method="POST" action="/addUser" enctype="multipart/form-data">
                            {{csrf_field()}}
                             <div class="form-group">
                                  <label for="first-name">First Name</label>
                                  <input type="text" class="form-control" name="first-name" placeholder="Enter first name">
                              </div>
                               <div class="form-group">
                                  <label for="last-name">Last Name</label>
                                  <input type="text" class="form-control" name="last-name" placeholder="Enter last name">
                              </div>
                              <div class="form-group">
                                <label for="company">Select a company</label>
                                  <select id="company_id" class="form-control mb-3" name="company_id">
                                        @foreach($companyArray as $data)
                                            <option value='{{$data->id}}'>
                                              {{$data->name}}
                                            </option>
                                        @endforeach
                                  </select>
                              </div>
                              <div class="form-group">
                                <label>Select events</label>
                                <hr/>
                                 @foreach($event as $data)
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="{{$data->id}}" name="event_ids[]" value="{{$data->id}}">
                                            <label class="custom-control-label" for="{{$data->id}}"> {{$data->name}}</label>
                                        </div>
                                 @endforeach
                              </div>
                              <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

