@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-secondary text-light">Edit users page</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form method="post" action="{{action('UserController@update', $id)}}">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="PATCH" />
                            <div class="form-group">
                                <label for="user-name">First Name</label>
                                <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}">
                            </div>
                            <div class="form-group">
                                <label for="user-name">Last Name</label>
                                <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}">
                            </div>
                            <label for="company">Select company</label>
                               <select id="company_id" class="form-control mb-3"
                                name="company_id">
                                  <option value="{{$user->company->id}}">                    {{$user->company->name}}
                                  </option>
                                  @foreach($companyArray as $data)
                                        <option value='{{$data->id}}'>
                                          {{$data->name}}
                                        </option>
                                  @endforeach
                              </select>
                              <div class="form-group">
                                    <label>Select events</label>
                                    <hr/>
                                    @foreach($event as $data)
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="{{$data->id}}" name="event_ids[]" value="{{$data->id}}"
                                            @foreach ($user->event as $option)
                                               @if($data->id == $option->id ) checked @endif
                                            @endforeach >
                                            <label class="custom-control-label" for="{{$data->id}}"> {{$data->name}}</label>
                                        </div>
                                    @endforeach
                              </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value=" Edit " />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

