<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = ['name'];


    public function user(){
        return $this->hasOne(User::class);
    }

}
