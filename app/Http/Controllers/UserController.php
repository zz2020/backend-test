<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\Company;

class UserController extends Controller
{
    public function index(){
        return view('users');
    }

    public function create() {
        $event = Event::all();
        // dd($event);
        return view('addUser', compact('event'));
    }

    public function show($id){
        $user = User::where('id', $id);
        $event = $user->get()->first()->event;
        $company = $user->get()->first()->company;

        if($event){
            return view('userDetails', compact('event', 'company'));
        }
    }

    public function store(Request $request) {
         $this->validate($request, [
            'first-name' => 'required',
            'last-name' => 'required',
            'company_id'   =>  'required',
        ]);
        $user = new User();
        $user->first_name = request('first-name');
        $user->last_name = request('last-name');
        $user->company_id = request('company_id');
        $user->save();

        $events = Event::findMany( $request->input("event_ids") );

        $user->event()->saveMany($events);

        return redirect('/')->with('success', 'New User added');
    }

    public function display(){
        $members = User::all();

        return view('users', compact('members'));
    }

    public function edit($id){
        $user = User::find($id);
        $event = Event::all();

        return view('editUser', compact('user', 'event', 'id'));
    }

    public function update(Request $request, $id){
         $this->validate($request, [
            'first_name'  => 'required',
            'last_name'  => 'required',
            'company_id'  => 'required',
        ]);

        $user = User::find($id);
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->company_id = $request->get('company_id');

        $user->save();

        $events = Event::findMany( $request->input("event_ids") );
        $user->event()->saveMany($events);

        return redirect('/')->with('success', 'Data updated');
    }


    public function destroy($id){
        $user = User::find($id);
            if($user !=null){
                $user->delete();
                return redirect('/')->with('success', 'User Deleted');
            }
        return redirect('/')->with('danger', 'wrong id');
    }

}
