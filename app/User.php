<?php

namespace App;
use App\Company;
use App\Event;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'company_id',
    ];

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function event(){
        return $this->hasMany(Event::class);
    }
}
